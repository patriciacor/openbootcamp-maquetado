import React from 'react';
import "../login/login.css";
import ImgLogin from "../../assets/login-img.png";

const Login = () => {
  return (
    <div className="Container">
        <div >
       <img src={ImgLogin} alt="" className="img"/>
        </div>
       <div className="text-login">
        <h1 className='title'> OpenBootcamp <strong>| Alumnos</strong></h1>
         <form>
            <label className='label'> Email
                <input className='inputs' type="text" placeholder="Introduce tu correo "/>            </label>
             <label className="label">Contraseña<input className='inputs' type="password" placeholder="Introduce tu contraseña"/></label>
            <label className="Rememberme"><input type="checkbox" className="checkbox1"/><p>Recuérdame</p></label>
           <a href="# " className='contraseña' >He olvidado la contraseña</a>
         <button>Iniciar Sesión</button>
          </form>
       </div>
       <footer>
        <p className='parrafo-1'>Copyright © 2021 Open Bootcamp SL, Imagina Group</p>
        <p className='parrafo-2'>Todos los derechos reservados.</p>
         <p className='parrafo-3'>Política de Privacidad</p>
       </footer>
    </div>
  )
}

export default Login